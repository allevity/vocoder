import os
import logging
import ffmpeg
from glob import glob
import youtube_dl

import cv2


class UIVideo(object):
    YOUTUBE_DIR = 'data/youtube'

    @classmethod
    def download_url(cls, uri):
        ydl_opts = {
            'outtmpl': os.path.join(cls.YOUTUBE_DIR, '%(title)s.%(ext)s'),
        }
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            logging.debug('Downloading uri={}'.format(uri))
            info_dict = ydl.extract_info(uri, download=True)
            # Bug? info_dict['ext'] can be .mp4 even though the mp4 was merged into .mkv
            # ex: https://www.youtube.com/watch?v=BZP1rYjoBgI
            # So we use glob
            filepath = None
            template = os.path.join(cls.YOUTUBE_DIR, '{title}.*'.format(title=info_dict['title']))
            for f in glob(template):
                if not f.endswith('.wav'):
                    filepath = f
                    break
            assert filepath, 'No not-wav file found  as {}'.format(template)
            logging.debug('Saved uri={} into {}'.format(uri, filepath))
        return filepath

    def play_video(self, videofile):
        logging.warning('play_video not stable yet; plays one and breaks')
        cap = cv2.VideoCapture(videofile)

        while (cap.isOpened()):
            ret, frame = cap.read()
            if not ret:
                break
            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()

    @staticmethod
    def vid2wav(filepath, wavfile):
        _ = ffmpeg.input(filepath).output(wavfile, ar=44100, ac=1).run()
