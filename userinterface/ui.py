import os
import csv
import logging

from appJar import gui
from scipy.io import wavfile
import simpleaudio as sa

from processing.vocoder import vocoder
from processing.play import play_sound, play_video
from userinterface.download import UIVideo


class UserInterface(UIVideo):
    WAV_OR_URL = 'Wav file path or URL'
    # DEFAULT_URL = 'https://www.youtube.com/watch?v=tPEE9ZwTmy0'  # .mp4
    DEFAULT_URL = 'https://www.youtube.com/watch?v=wUF9DeWJ0Dk'    # .mvk
    DEFAULT_WAV = 'data/testfile.wav'                              # .wav
    YOUTUBE_DIR = 'data/youtube'
    URL2FILE_CSV = os.path.join(YOUTUBE_DIR, 'url2filepath.csv')
    URL2FILEPATH = {}

    def __init__(self):
        self.app = gui()  # create a GUI variable called app
        self.wavfile = self.DEFAULT_URL  # or self.DEFAULT_WAV
        self.waveform = None
        self.fs = None
        self.f0_floor = 50.0
        self.f0_ceil = 600.0
        self.play_objs = []  # contains the play_obj obtained when clicking Play
        self._load_csv()
        self.video_file = ''

    def run(self):
        # add & configure widgets - widgets get a name, to help referencing them later
        app = self.app
        app.setBg("OldLace")  # http://appjar.info/pythonBasics/#colour-map

        app.setSize(600, 300)
        app.addLabel("title", "Vocoder")
        app.setLabelBg("title", "orange")
        app.addMessage('InputMessage','Input wav path or Youtube URL:')
        app.setMessageWidth('InputMessage', 500)

        app.addTextArea(self.WAV_OR_URL)
        app.setTextArea(self.WAV_OR_URL, self.wavfile, end=True, callFunction=True)
        app.setTextAreaHeight(self.WAV_OR_URL, 10)

        app.addButtons(["Load", "Close"], self._press)
        self._add_parameter('f0_floor', _from=1, _to=200, curr=self.f0_floor)
        self._add_parameter('f0_ceil', _from=50, _to=2000, curr=self.f0_ceil)
        app.go()

    def _add_parameter(self, title, _from=1, _to=200, curr=50.0):
        self.app.addLabelScale(title)
        self.app.showScaleValue(title, show=True)
        self.app.setScaleRange(title, _from, _to, curr=curr)

    def _press(self, button):
        if button == "Close":
            self.app.stop()
            return

        # Get either as Label or TextArea
        try:
            uri = self.app.getEntry(self.WAV_OR_URL)
        except Exception:
            uri = self.app.getTextArea(self.WAV_OR_URL)

        if '://' in uri:
            self.video_file, wavfile = self._uri2wav(uri)
            # self.play_video(self.video_file)
            self._local_wav_behaviour(wavfile)
        else:
            self._local_wav_behaviour(uri)

        # Only add the 'Play' buttons after a file was loaded
        try:
            # Has been called
            _ = self.app.getButton('Robot')
        except Exception:
            # Hasn't been called
            self.app.addButtons(["Robot", "Normal", "Stop"], self._play)

        # Cleaning the play objects
        for play_obj in self.play_objs:
            try:
                if not play_obj.isplaying():
                    self.play_objs.remove(play_obj)
            except AttributeError:
                # Might break if we reload
                self.play_objs.remove(play_obj)

    @classmethod
    def _load_csv(cls):
        if not os.path.exists(cls.YOUTUBE_DIR):
            os.makedirs(cls.YOUTUBE_DIR)
        try:
            with open(cls.URL2FILE_CSV, 'r') as f:
                for url, filepath in csv.reader(f):
                    cls.URL2FILEPATH[url] = filepath
        except FileNotFoundError:
            if not os.path.exists(cls.YOUTUBE_DIR):
                os.mkdir(cls.YOUTUBE_DIR)
            with open(cls.URL2FILE_CSV, 'a'):
                os.utime(cls.URL2FILE_CSV, None)
        logging.debug('Loaded CSV')

    @classmethod
    def _write_csv(cls):
        with open(cls.URL2FILE_CSV, 'w') as f:
            for uri in cls.URL2FILEPATH:
                f.write('{},{}\n'.format(uri, cls.URL2FILEPATH[uri]))
        logging.debug('Saved CSV')

    @classmethod
    def _uri2wav(cls, uri=DEFAULT_URL, video_format='.mp4'):

        try:
            filepath = cls.URL2FILEPATH[uri]
            logging.info('filepath={} found for URI={}'.format(filepath, uri))
        except KeyError:
            filepath = cls.download_url(uri)
            cls.URL2FILEPATH[uri] = filepath
            logging.info(cls.URL2FILEPATH)
            cls._write_csv()

        base, _ = os.path.splitext(filepath)
        # to wav
        wavfile = '{}_audio.wav'.format(base)
        if not os.path.exists(wavfile):
            logging.debug('File {} does not exist, not calling ffmpeg'.format(wavfile))
            cls.vid2wav(filepath, wavfile)
        return filepath, wavfile
        # self.play_video(filepath)

    def _local_wav_behaviour(self, wav_filepath):
        self.wavfile = wav_filepath
        self.fs, self.waveform = wavfile.read(self.wavfile)
        logging.debug("Wavefile uploaded from", self.wavfile)

    def _play(self, button):
        if button == 'Robot':
            robot_like, fs = vocoder(
                self.waveform, self.fs,
                f0_floor=self.app.getScale("f0_floor"),
                f0_ceil=self.app.getScale("f0_ceil"),
                channels_in_octave=2,
            )
            play_obj = play_sound(robot_like, fs, wait=False)
            self.play_objs.append(play_obj)
        elif button == 'Normal':
            play_obj = play_sound(self.waveform, self.fs, wait=False)
            self.play_objs.append(play_obj)
            # self.play_video(self.video_file)
        else:
            sa.stop_all()

