import logging
from userinterface.ui import UserInterface

logging.basicConfig(level=logging.INFO)


def run():
    ui = UserInterface()
    ui.run()


if __name__ == '__main__':
    run()
