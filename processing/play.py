import numpy as np
import simpleaudio as sa
from scipy.io import wavfile
import cv2
import logging

from processing.vocoder import vocoder


def read_wavfile(filepath):
    """
    :param str filepath: path to a .wav file
    :return array-of-floats y: waveform
    """
    sr, y = wavfile.read(filepath)
    return y, sr


def play_sound(waveform, fs, wait=True):
    # Convert to 16-bit data
    waveform = waveform.astype(np.int16)

    # Start playback
    play_obj = sa.play_buffer(waveform, 1, 2, fs)
    if wait:
        # Wait for playback to finish before exiting
        return play_obj.wait_done()

    return play_obj


def play_video(videofile):
    logging.warning('play_video not stable yet; plays one and breaks')
    cap = cv2.VideoCapture(videofile)

    while (cap.isOpened()):
        ret, frame = cap.read()
        if not ret:
            break
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


def play_vocoder(filepath='data/testfile.wav'):
    # Read the mp3
    y, fs = read_wavfile(filepath)

    # Modify the soundwave
    robot_like, fs = vocoder(y, fs)

    # Play the new sound
    play_sound(robot_like, fs)