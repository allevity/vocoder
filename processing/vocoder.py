import pyworld as pw


def vocoder(x, fs, f0_floor=50.0, f0_ceil=600.0, channels_in_octave=2):
    x = x.astype('double')
    _f0, t = pw.dio(x, fs, f0_floor=f0_floor, f0_ceil=f0_ceil, channels_in_octave=channels_in_octave,
                    frame_period=pw.default_frame_period)
    _sp = pw.cheaptrick(x, _f0, t, fs)
    _ap = pw.d4c(x, _f0, t, fs)
    _y = pw.synthesize(_f0, _sp, _ap, fs, pw.default_frame_period)
    return _y, fs

