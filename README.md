# vocoder

Python vocoder. Currrently deals with local paths or youtube URLs.
Downloads and extracts the sound into a .wav to process it. 

## Requirements
Your python3 needs to be installed with tkinter, so that 
```
python -m tkinter -c 'tkinter._test()'
```
should open a `Click me` window. 

## Install
You may want to create a new virtual environment first. 
Then clone the repo and install required packages:
```
git clone git@bitbucket.org:allevity/vocoder.git
cd vocoder/
python -m pip install .  
```
 
## Run
Execute
```
python .
```

## OS

Only tested on Mac OS High Sierra 10.13.6, Processor 2.7 GHz Intel Core i7.