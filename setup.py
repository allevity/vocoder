import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='vocoderemix',
    version='0.0.1',
    author='allevity@gmail.com',
    description='Vocoder project to remix youtube videos',
    long_description=long_description,
    url="https://bitbucket.org/allevity/vocoder",
    install_requires=[
        'ffmpeg-python==0.2.0',
        'youtube-dl==2019.11.28',
        'appJar==0.94.0',
        'numpy==1.17.4',
        'opencv-python==4.1.2.30',
        'pyworld==0.2.8',
        'scipy==1.3.3',
        'simpleaudio==1.0.4',
    ],
    packages=setuptools.find_packages(include=[]),
    python_requires='>=3.6',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Mac OS X",
    ],
)
